<?php require_once "./code.php" ?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>S01: PHP Basics and Selection Control Structures</title>
	</head>
	<body>
		<!-- <h1>Hello World!</h1> -->

		<h1>Echoing Values</h1>
		<!-- Variables can be used to output the data in double quotes while single quotes do not -->
<!-- 		<p>
			<?php   echo 'Good day $name! Your given email is $email' ?>
		</p> -->

		<p>
			<?php   echo "Good day $name! Your given email is $email" ?>
		</p>

		<p>
			<?php   echo PI ?> <!-- //define -->
		</p>

		<p>
			<?php   echo "$address" ?>
		</p>

		<h1>Data Types</h1>
		<p>
			<?php   echo $age ?>
			<?php   echo $headcount ?>
		</p>

<!-- To output the value of an object property, the arrow notation can be used -->
		<p>
			<?php   echo $gradesObj->firstGrading; ?>
			<?php   echo $personObj->address->state; ?>		
		</p>

		<!-- Will not print out an output on the web page -->
		<p><?php echo $hasTravelledAbroad; ?></p>
		<p><?php echo $girlfriend; ?></p>

		<!-- var_dump to see more details info of the variable -->
		<p><?php echo gettype($hasTravelledAbroad); ?></p>
		<p><?php echo var_dump($hasTravelledAbroad); ?></p>
		<p><?php echo gettype($girlfriend); ?></p>
		<p><?php echo var_dump($girlfriend); ?></p>

		<p><?php echo $grades[2]; ?></p>

		<h1>Operators</h1>

		<h2>Arithmetic Operators</h2>

		<p>Sum: <?php echo $x + $y; ?> </p>

		<h2>Equality Operators</h2>

		<p>Loose Equality: <?php echo var_dump($x == 1342.14)?></p>
		<p>Loose Equality: <?php echo var_dump($x != 1342.14)?></p>

		<h2>Greater/Lesser Operators</h2>
		<p>isGreater: <?php echo var_dump($x > $y); ?></p>
		
		<h2>Logical Operators</h2>
		<p>Are All Requirements Met: <?php echo var_dump($isLegalAge && $isRegistered); ?></p>
		<p>Are All Requirements Met: <?php echo var_dump(!$isLegalAge && !$isRegistered); ?></p>

		<h1>Function</h1>
		<p>Full Name: <?php echo getFullName('John', 'D.', 'Smith'); ?></p>

		<h2>If-ElseIf-Else</h2>
		<p><?php echo determineTyphoonIntensity(35); ?></p>

		<h2>Ternary</h2>
		<p><?php echo var_dump(isUnderAge(78)); ?></p>

		<h2>Switch</h2>

		<p><?php echo determineComputerUser(4) ?></p>

		<h2>Try-Catch-Finally</h2>
		<p><?php echo greeting('hello') ?></p>


		<h2>Full Address</h2>
		<p><?php echo getFullAddress('Philippines', 'Metro Manila', 'Quezon City', '3F Caswyn Bldg., Timog Avenue'); ?></p>
		<p><?php echo getFullAddress('Philippines', 'Metro Manila', 'Makati City', '3F Enzo Bldg., Buendia Avenue'); ?></p>
		
		<h2>Letter-Based Grading</h2>
		<p><?php echo getLetterGrade(87); ?></p>
		<p><?php echo getLetterGrade(94); ?></p>
		<p><?php echo getLetterGrade(74); ?></p>






	</body>
</html>